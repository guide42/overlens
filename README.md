# Overlens

## Development

Add [lens test wallet](https://github.com/aave/lens-protocol/blob/main/helpers/test-wallets.ts)
to MetaMask. Choose the fourth one to interact with the protocol as normal user.

Start a hardhat node with lens deployed and unpaused, a PostgreSQL database
with PostgREST api, an authentication server and a user interface.

```shell
$ docker-compose up
```

Wait until it ends and create a profile:

```shell
$ docker-compose exec lens npx hardhat --network localhost create-profile --handle example
```

Access the UI at http://localhost:3080 or OpenAPI at http://localhost:8080.
