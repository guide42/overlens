create table auth.sessions (
  id char(21) not null primary key,
  address char(42) not null,
  role auth.role not null,
  revoked boolean not null default false,
  ts timestamp with time zone default now(),

  constraint session_id check (length(id) = 21),
  constraint session_address check (length(address) = 42 and address::text ~ '^0x[0-9a-fA-F]+$')
);
