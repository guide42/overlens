create function auth.check_token() returns void language plpgsql as $$
begin
  if auth.current_address() = '' then
    raise insufficient_privilege using
      message = 'Unauthorized', hint = 'You are no one';
  end if;
end $$;

grant execute on function auth.check_token() to :"anonymous", :"author";
