create schema auth;
grant usage on schema auth to :"anonymous", :"author";

\ir ./check_token.sql
\ir ./current_address.sql

\ir ./role.sql
\ir ./challenges.sql
\ir ./sessions.sql
