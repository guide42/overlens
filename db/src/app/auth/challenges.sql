create table auth.challenges (
  id char(21) not null primary key,
  address char(42) not null,
  role auth.role not null default 'author',
  ts timestamp with time zone default now(),

  constraint challenge_id check (length(id) = 21),
  constraint challenge_address check (length(address) = 42 and address::text ~ '^0x[0-9a-fA-F]+$')
);
