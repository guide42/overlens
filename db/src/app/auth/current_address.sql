create function auth.current_address() returns text language plpgsql as $$
begin
	return current_setting('request.jwt.claims', true)::json->'address';
exception
	-- handle unrecognized configuration parameter error
	when undefined_object then return '';
end $$;

grant execute on function auth.current_address() to :"anonymous", :"author";
