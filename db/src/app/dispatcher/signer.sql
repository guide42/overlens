create table dispatcher.signer (
  address char(42) not null primary key,
  higher_nonce int not null default 0,
  updated_ts timestamp with time zone default now(),

  constraint signer_address check (length(address) = 42 and address::text ~ '^0x[0-9a-fA-F]+$')
);

create function dispatcher.update_ts() returns trigger as $$
begin
  if row(NEW.*) is distinct from row(OLD.*) then
    NEW.updated_ts = now();
    return NEW;
  else
    return OLD;
  end if;
end $$ language 'plpgsql';

create trigger signer_updated before update on dispatcher.signer
  for each row execute function dispatcher.update_ts();
