create table dispatcher.dispatcher (
  profile_id int not null primary key,
  address char(42) not null references dispatcher.signer(address),

  constraint dispatcher_address check (length(address) = 42 and address::text ~ '^0x[0-9a-fA-F]+$')
);
