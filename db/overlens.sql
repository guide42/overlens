\set ON_ERROR_STOP on

-- load env vars
\set authenticator_password `echo $OVERLENS_AUTHENTICATOR_PASSWORD`
\set authenticator `echo $OVERLENS_AUTHENTICATOR_USER`
\set anonymous `echo $OVERLENS_ANONYMOUS_ROLE`
\set author `echo $OVERLENS_AUTHOR_ROLE`
\set api `echo $OVERLENS_API_SCHEMA`

-- load extensions
create extension if not exists pgcrypto;

-- limit privileges of functions
alter default privileges revoke execute on functions from public;

begin;

  create role :"authenticator" noinherit login
    password :'authenticator_password';

  create role :"anonymous" noinherit nologin;
  create role :"author" noinherit nologin;

  grant :"anonymous" to :"authenticator";
  grant :"author" to :"authenticator";

  -- application features
  \ir ./src/app/auth/schema.sql
  \ir ./src/app/dispatcher/schema.sql

  -- public api
  create schema :"api";
  grant usage on schema :"api" to :"anonymous", :"author";
  grant execute on all functions in schema :"api" to :"author";

commit;
