import {Interface} from "@ethersproject/abi"

import LensHubAbi from "../abi/LensHub.json"

export const LensHub = new Interface(LensHubAbi)
