import {writable} from "svelte/store"
import networks from "./networks"

function createEthereumStore() {
  const provider = window.ethereum

  const {subscribe, update} = writable({
    provider,
    connected: false,
    defaultAccount: null,
    accounts: [],
    chainId: 0,
  })

  const onChainId = chainId => update(prevState => ({
    ...prevState,
    chainId: typeof chainId === 'string' && chainId.substring(0, 2) === '0x'
      ? parseInt(chainId, 16)
      : chainId,
  }))

  const onAccounts = accounts => {
    const defaultAccount = accounts.length > 0 ? accounts[0] : null
    update(prevState => ({
      ...prevState,
      connected: accounts.length > 0,
      defaultAccount,
      accounts,
    }))
    localStorage.setItem('accounts', JSON.stringify(accounts))
    return defaultAccount
  }

  function listen() {
    const encodedAccounts = localStorage.getItem('accounts')
    if (encodedAccounts) {
      const storedAccounts = JSON.parse(encodedAccounts)
      if (storedAccounts.length > 0) {
        update(prevState => ({
          ...prevState,
          connected: true,
          defaultAccount: storedAccounts[0],
          accounts: storedAccounts,
        }))
      }
    }
    if (provider) {
      provider.on("accountsChanged", onAccounts)
      provider.on("chainChanged", onChainId)
      provider.request({method: "eth_chainId"}).then(onChainId)
    }
  }

  function connect() {
    if (provider) {
      return provider.request({method: "eth_requestAccounts"}).then(onAccounts)
    }
    return Promise.reject(new Error('Unknown provider'))
  }

  function switchTo(network) {
    return provider.request({
      method: 'wallet_switchEthereumChain',
      params: [{chainId: `0x${networks[network].chainId.toString(16)}`}],
    }).catch(err => {
      if (err.code === 4902 || /Unrecognized chain ID/.test(err.message)) {
        return provider.request({
          method: 'wallet_addEthereumChain',
          params: [{
            chainId: `0x${networks[network].chainId.toString(16)}`,
            chainName: networks[network].name,
            rpcUrls: networks[network].rpcUrls,
          }],
        })
      }
      return Promise.reject(err)
    })
  }

  function disconnect() {
    localStorage.removeItem('accounts')
    update(prevState => ({
      ...prevState,
      connected: false,
      defaultAccount: null,
      accounts: [],
    }))
  }

  return {
    subscribe,
    listen,
    connect,
    disconnect,
    switchTo,
  }
}

function createLocalStorageStore(key, initial) {
  const store = writable(initial, set => {
    const value = localStorage.getItem(key)
    if (value) {
      set(JSON.parse(value))
    }

    const handleStorage = event => {
      if (event.key === key) {
        if (event.newValue) {
          const parsed = JSON.parse(event.newValue)
          set(parsed)
        } else {
          set(initial)
        }
      }
    }

    window.addEventListener('storage', handleStorage)
    return () => window.removeEventListener("storage", handleStorage)
  })

  store.subscribe(value => {
    if ([null, undefined].includes(value)) {
      localStorage.removeItem(key)
    } else {
      const encoded = JSON.stringify(value)
      localStorage.setItem(key, encoded)
    }
  })

  return store
}

export const ethereum = createEthereumStore()

export const auth = createLocalStorageStore('auth', {
  accessToken: undefined,
  refreshToken: undefined,
})

export const hub = writable({
  name: undefined,
  symbol: undefined,
})

export const account = writable({
  loaded: false,
  profiles: [], // list of {profileId, handle}

  // the following are indexed by profileId
  dispatchers: {},
  metadata: {},
})
