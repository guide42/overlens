import App from "./App.svelte"

export default new App({
  target: document.body,
  props: {
    apiUrl: process.env.OVERLENS_API_URL,
    authUrl: process.env.OVERLENS_AUTH_URL,
    network: process.env.OVERLENS_NETWORK,
    hubAddress: process.env.OVERLENS_HUB_ADDRESS,
  }
})
