function parseChainId(chainId) {
  if (typeof chainId === 'number') {
    return chainId
  }
  if (typeof chainId === 'string') {
    if (chainId.substring(0, 2) === '0x') {
      return parseInt(chainId, 16)
    }
    return parseInt(chainId)
  }
  return null
}

export default {
  development: {
    chainId: parseChainId(process.env.OVERLENS_DEVELOPMENT_CHAIN_ID) ?? 31337,
    name: 'Development',
    rpcUrls: [process.env.OVERLENS_DEVELOPMENT_RPC_URL ?? 'http://127.0.0.1:8545'],
    linkToHub: address => `#/hubs/${address}`,
  },
  maticmum: {
    chainId: 80001,
    name: 'Mumbai',
    rpcUrls: ['https://rpc-mumbai.maticvigil.com', 'https://matic-mumbai.chainstacklabs.com'],
    linkToHub: address => `https://mumbai.polygonscan.com/address/${address}`,
  },
  matic: {
    chainId: 137,
    name: 'Polygon',
    rpcUrls: ['https://rpc-mainnet.maticvigil.com', 'https://matic-mainnet.chainstacklabs.com'],
    linkToHub: address => `https://polygonscan.com/address/${address}`,
  },
}
