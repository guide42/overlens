import replace from "@rollup/plugin-replace"
import svelte from "rollup-plugin-svelte"
import css from "rollup-plugin-css-only"
import json from "@rollup/plugin-json"
import {nodeResolve} from "@rollup/plugin-node-resolve"
import commonjs from "@rollup/plugin-commonjs"
import {terser} from "rollup-plugin-terser"
import serve from "rollup-plugin-serve"

const IsDevelopment = process.env.NODE_ENV === "development"

export default {
  input: "src/main.js",
  output: {
    sourcemap: IsDevelopment,
    format: "iife",
    name: "overlens",
    file: "public/dist/overlens.js"
  },
  cache: IsDevelopment,
  plugins: [
    replace({
      preventAssignment: true,
      "process.env.OVERLENS_API_URL": JSON.stringify(process.env.OVERLENS_API_URL),
      "process.env.OVERLENS_AUTH_URL": JSON.stringify(process.env.OVERLENS_AUTH_URL),
      "process.env.OVERLENS_NETWORK": JSON.stringify(process.env.OVERLENS_NETWORK),
      "process.env.OVERLENS_HUB_ADDRESS": JSON.stringify(process.env.OVERLENS_HUB_ADDRESS),
      "process.env.OVERLENS_DEVELOPMENT_CHAIN_ID": JSON.stringify(process.env.OVERLENS_DEVELOPMENT_CHAIN_ID),
      "process.env.OVERLENS_DEVELOPMENT_RPC_URL": JSON.stringify(process.env.OVERLENS_DEVELOPMENT_RPC_URL),
    }),
    svelte({compilerOptions: {dev: IsDevelopment}}),
    css({output: "overlens.css"}),
    json({compact: true}),
    nodeResolve({
      browser: true,
      mainFields: ["browser"],
      dedupe: ["svelte"],
    }),
    commonjs({sourceMap: IsDevelopment}),
    terser(),
    IsDevelopment && serve({
      contentBase: "public",
      port: process.env.NGINX_PORT,
    })
  ],
  watch: {clearScreen: false}
}
