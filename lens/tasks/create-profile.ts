import {task} from 'hardhat/config'
import {LensHub__factory} from '../typechain-types'
import {CreateProfileDataStruct} from '../typechain-types/LensHub'
import {waitForTx, initEnv, getAddrs, ZERO_ADDRESS} from './helpers/utils'

task('create-profile', 'creates a profile').addParam('handle', 'Profile\'s handle').setAction(async ({handle}, hre) => {
  const [governance, , user] = await initEnv(hre)
  const addrs = getAddrs()
  const lensHub = LensHub__factory.connect(addrs['lensHub proxy'], governance)

  console.log(`Whitelisting ${user.address}`)

  await waitForTx(
    lensHub.whitelistProfileCreator(user.address, true)
  )

  const profile: CreateProfileDataStruct = {
    to: user.address,
    handle,
    imageURI: 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==',
    followModule: ZERO_ADDRESS,
    followModuleInitData: [],
    followNFTURI: 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==',
  }

  console.log(`Creating ${handle}`)

  await waitForTx(
    lensHub.connect(user).createProfile(profile)
  )

  const profileId = await lensHub.getProfileIdByHandle(handle)
  console.log(`Profile Id : ${profileId}`)

  const owner = await lensHub.ownerOf(profileId)
  console.log(`Owner : ${owner}`)

  const balance = await lensHub.balanceOf(owner)
  console.log(`Balance : ${balance}`)
})
