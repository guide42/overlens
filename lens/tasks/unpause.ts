import {task} from 'hardhat/config'
import {LensHub__factory} from '../typechain-types'
import {ProtocolState, waitForTx, initEnv, getAddrs} from './helpers/utils'

task('unpause', 'unpauses the protocol').setAction(async ({}, hre) => {
  const [governance] = await initEnv(hre)
  const addrs = getAddrs()
  const lensHub = LensHub__factory.connect(addrs['lensHub proxy'], governance)
  const prevState = await lensHub.getState()
  if (prevState === ProtocolState.Unpaused) {
    console.log('Already unpaused')
  } else {
    await waitForTx(
      lensHub.setState(ProtocolState.Unpaused)
    )
    const afterState = await lensHub.getState()
    console.log(`Protocol state ${afterState}`)
  }
})
