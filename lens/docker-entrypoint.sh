#!/bin/bash

set -m

npm run hardhat node &

echo "Waiting for Hardhat local node"
while ! netstat -tna | grep 'LISTEN\>' | grep -q ':8545\>'; do
  sleep 2
done

echo "Deploying Lens Protocol to local node"
npm run full-deploy-local

echo "Unpausing protocol"
npx hardhat --network localhost unpause

fg
