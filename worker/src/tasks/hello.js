const provider = require('./lib/provider')

module.exports = async (payload, helpers) => {
  const {name} = payload
  helpers.logger.info(`${provider.hello}, ${name}`)
}
