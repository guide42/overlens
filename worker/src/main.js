const PgBoss = require('pg-boss')
const {ENV} = require('./env')
const logger = require('./logger').child({mod: 'worker'})
const db = require('./db')

const boss = new PgBoss({
  db,
  schema: 'worker',
  application_name: 'overlens',
  max: 10,
  deleteAfterDays: 14,
  maintenanceIntervalMinutes: 1,
})

boss.on('error', err => logger.error({err, env: ENV}))

if (require.main === module) {
  boss.start().then(
    () => logger.info({msg: 'Worker connected', env: ENV}),
    err => logger.error({err, env: ENV})
  )
}

module.exports = boss
