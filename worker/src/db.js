const {Pool} = require('pg')
const {DB_URL} = require('./env')
const logger = require('./logger').child({mod: 'db'})

const dbpool = new Pool({connectionString: DB_URL})

process.on('beforeExit', () => {
  dbpool.end()
})

dbpool.on('error', err => {
  logger.error({
    msg: 'Unexpected database error',
    err,
  })
  process.exit(-1)
})

function executeSql(text, values) {
  return dbpool.query(text, values)
}

module.exports = {
  dbpool,
  executeSql,
}
