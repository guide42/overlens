module.exports = {
  ENV: process.env.NODE_ENV,
  DB_URL: process.env.OVERLENS_DB_URL,
}
