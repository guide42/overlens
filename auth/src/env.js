module.exports = {
  ENV: process.env.NODE_ENV,
  PORT: process.env.OVERLENS_AUTH_PORT ?? 3001,
  DB_URL: process.env.OVERLENS_DB_URL,
  MESSAGE_TEMPLATE: process.env.OVERLENS_MESSAGE_TEMPLATE,
  JWT_SECRET: process.env.OVERLENS_JWT_SECRET,
}
