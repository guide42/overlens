module.exports = class Challenges {
  constructor(db, options) {
    this.db = db
    this.latestInterval = options?.latestInterval ?? '5 minutes'
    this.latestLimit = options?.latestLimit ?? 3
  }

  appendToAddress(address, id) {
    return this.db.query({
      name: 'insert-challenge',
      text: `
        insert into auth.challenges (id, address) values ($1, $2)
        returning (id, address, role, ts)
      `,
      values: [id, address],
    })
  }

  findLastestByAddress(address) {
    return this.db.query({
      name: 'select-latest-challenges',
      text: `
        select id, address, role, ts
        from auth.challenges
        where address = $1
        and ts >= now() - interval '${this.latestInterval}'
        order by ts desc
        limit ${this.latestLimit}
      `,
      values: [address],
    })
  }

  removeFromAddress(address, id) {
    return this.db.query({
      name: 'delete-challenge-from-address',
      text: `
        delete from auth.challenges
        where id = $1
        and address = $2
      `,
      values: [id, address],
    })
  }
}
