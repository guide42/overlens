module.exports = class Sessions {
  constructor(db, options) {
    this.db = db
  }

  createForAddress(id, address, role) {
    return this.db.query({
      name: 'insert-session-for-address',
      text: `
        insert into auth.sessions (id, address, role) values ($1, $2, $3)
        returning (id, address, role, revoked, ts)
      `,
      values: [id, address, role],
    })
  }

  findByAddress(id, address) {
    return this.db.query({
      name: 'select-sessions-by-address',
      text: `
        select id, address, role, revoked, ts
        from auth.sessions
        where id = $1
        and address = $2
        and revoked = false
        order by ts desc
      `,
      values: [id, address],
    })
  }

  markAsRevoked(id) {
    return this.db.query({
      name: 'update-revoke-session',
      text: `
        update auth.sessions
        set revoked = true
        where id = $1
      `,
      values: [id],
    })
  }
}
