const {string, pattern, object, size, enums, type} = require('superstruct')

const Id = size(string(), 21)
const HexString = pattern(string(), /^0x[0-9a-fA-F]+$/)
const Signature = size(HexString, 132)
const Address = size(HexString, 42)
const Message = string()
const Role = enums(['author'])
const Token = pattern(string(), /^.+\..+\..+$/)
const AccessTokenData = type({role: Role, address: Address})
const RefreshTokenData = type({sessionId: Id, address: Address})

const Authenticated = object({
  accessToken: Token,
  refreshToken: Token,
})

module.exports = {
  Signature,
  Address,
  Message,
  Role,
  Token,
  AccessTokenData,
  RefreshTokenData,
  Authenticated,
}
