const {TextEncoder} = require('util')
const {assert, create} = require('superstruct')
const {jwtVerify, SignJWT} = require('jose')
const {nanoid} = require('nanoid')

const {SessionNotFoundError} = require('../errors')
const {Token, AccessTokenData, RefreshTokenData, Authenticated} = require('../models')

module.exports = (sessions, jwtSecret) => async function refresh(token) {
  assert(token, Token)
  const {payload} = await jwtVerify(token, new TextEncoder().encode(jwtSecret))
  assert(payload, RefreshTokenData)
  const address = payload.address.toLowerCase()
  const session = await sessions.findByAddress(payload.sessionId, address)
  if (session.rowCount === 0) {
    throw new SessionNotFoundError(refreshToken)
  }
  const accessTokenData = create({role: session.rows[0].role, address}, AccessTokenData)
  const accessToken = await new SignJWT(accessTokenData)
    .setProtectedHeader({alg: 'HS256'})
    .setIssuedAt()
    .setExpirationTime('1h')
    .sign(new TextEncoder().encode(jwtSecret))
  const sessionId = nanoid()
  await sessions.createForAddress(sessionId, address, 'author')
  const refreshTokenData = create({sessionId, address}, RefreshTokenData)
  const refreshToken = await new SignJWT(refreshTokenData)
    .setProtectedHeader({alg: 'HS256'})
    .setIssuedAt()
    .setExpirationTime('1d')
    .sign(new TextEncoder().encode(jwtSecret))
  const authenticated = create({accessToken, refreshToken}, Authenticated)
  await sessions.markAsRevoked(payload.sessionId)
  return authenticated
}
