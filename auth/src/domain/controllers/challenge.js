const {format} = require('util')
const {nanoid} = require('nanoid')
const {assert, create} = require('superstruct')

const {Address, Message} = require('../models')

module.exports = (challenges, template) => async function challenge(address) {
  assert(address, Address)
  address = address.toLowerCase()
  const challengeId = nanoid()
  await challenges.appendToAddress(address, challengeId)
  const message = format(template, challengeId)
  return create(message, Message)
}
