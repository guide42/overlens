const {TextEncoder} = require('util')
const {assert, is} = require('superstruct')
const {jwtVerify} = require('jose')

const {Token, RefreshTokenData} = require('../models')

module.exports = (sessions, jwtSecret) => async function verify(token) {
  assert(token, Token)
  let tokenData = {}
  try {
    const {payload} = await jwtVerify(token, new TextEncoder().encode(jwtSecret))
    tokenData = {...payload}
  } catch (err) {
    return false
  }
  if (is(tokenData, RefreshTokenData)) {
    const session = await sessions.findByAddress(tokenData.sessionId, tokenData.address)
    if (session.rowCount > 0) {
      return true
    }
  }
  return !!tokenData.address
}
