const {format, TextEncoder} = require('util')
const {assert, create} = require('superstruct')
const {verifyMessage} = require('@ethersproject/wallet')
const {SignJWT} = require('jose')
const {nanoid} = require('nanoid')

const {SignatureNotVerifiedError} = require('../errors')
const {Address, Signature, AccessTokenData, Authenticated, RefreshTokenData} = require('../models')

module.exports = (challenges, sessions, template, jwtSecret) => async function signin(address, signature) {
  assert(address, Address)
  assert(signature, Signature)
  address = address.toLowerCase()
  const latest = await challenges.findLastestByAddress(address)
  const challenge = latest.rows.find(challenge => {
    const message = format(template, challenge.id)
    const recovered = verifyMessage(message, signature)
    return recovered.toLowerCase() === address
  })
  if (!challenge) {
    throw new SignatureNotVerifiedError(signature)
  }
  const accessTokenData = create({role: challenge.role, address}, AccessTokenData)
  const accessToken = await new SignJWT(accessTokenData)
    .setProtectedHeader({alg: 'HS256'})
    .setIssuedAt()
    .setExpirationTime('1h')
    .sign(new TextEncoder().encode(jwtSecret))
  const sessionId = nanoid()
  await sessions.createForAddress(sessionId, address, 'author')
  const refreshTokenData = create({sessionId, address}, RefreshTokenData)
  const refreshToken = await new SignJWT(refreshTokenData)
    .setProtectedHeader({alg: 'HS256'})
    .setIssuedAt()
    .setExpirationTime('1d')
    .sign(new TextEncoder().encode(jwtSecret))
  const authenticated = create({accessToken, refreshToken}, Authenticated)
  await challenges.removeFromAddress(address, challenge.id)
  return authenticated
}
