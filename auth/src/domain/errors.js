class SignatureNotVerifiedError extends Error {
  constructor(signature) {
    super('Signature could not be verified, could be old or non-existent')
    this.signature = signature
  }
}

class SessionNotFoundError extends Error {
  constructor(refreshToken) {
    super('Session not found, could be old or revoked')
    this.refreshToken = refreshToken
  }
}

module.exports = {
  SignatureNotVerifiedError,
  SessionNotFoundError,
}
