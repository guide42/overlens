const http = require('http')
const {ENV, PORT} = require('./env')
const logger = require('./logger').child({mod: 'server'})
const api = require('./api')
const server = http.createServer(api)

if (require.main === module) {
  server.listen(PORT, () => {
    logger.info({
      msg: 'Authorization server running',
      env: ENV,
      port: PORT,
    })
  })
}

module.exports = server
