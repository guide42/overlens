const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const {StructError} = require('superstruct')
const {JWTInvalid, JWTExpired} = require('jose')
const logger = require('./logger').child({mod: 'api'})
const version = require('../package.json').version
const errors = require('./domain/errors')
const services = require('./services')
const api = express()

api.disable('x-powered-by')
api.set('etag', false)
api.set('trust proxy', true)
api.use(helmet())
api.use(cors())
api.use(express.json())

api.use((req, res, next) => {
  logger.info({
    msg: 'Request',
    method: req.method,
    url: req.url,
    body: req.body,
    contentType: req.header('content-type'),
    contentLength: req.header('content-length'),
  })
  next()
})

api.post('/challenge', async (req, res, next) => {
  try {
    const address = req.body.address
    const message = await services.challenge(address)

    logger.info({
      msg: 'Challenge accepted',
      address,
    })

    res.status(200).json({message})
  } catch (err) {
    if (err instanceof StructError) {
      res.status(400).json({err: 'Invalid address'})
    } else {
      next(err)
    }
  }
})

api.post('/signin', async (req, res, next) => {
  try {
    const address = req.body.address
    const signature = req.body.signature
    const tokens = await services.signin(address, signature)

    logger.info({
      msg: 'Authenticated',
      address,
    })

    res.status(200).json(tokens)
  } catch (err) {
    if (err instanceof StructError) {
      res.status(400).json({err: 'Invalid address or signature'})
    } else if (err instanceof errors.SignatureNotVerifiedError) {
      res.status(403).json({err: 'Forbidden'})
    } else {
      next(err)
    }
  }
})

api.post('/verify', async (req, res, next) => {
  try {
    const token = req.body.token
    const verified = await services.verify(token)

    res.status(200).json({verified})
  } catch (err) {
    if (err instanceof StructError) {
      res.status(400).json({err: 'Invalid token'})
    } else {
      next(err)
    }
  }
})

api.post('/refresh', async (req, res, next) => {
  try {
    const refreshToken = req.body.refreshToken
    const tokens = await services.refresh(refreshToken)

    logger.info({
      msg: 'Authentication refreshed',
      refreshToken,
    })

    res.status(200).json(tokens)
  } catch (err) {
    if (err instanceof StructError || err instanceof JWTInvalid) {
      res.status(400).json({err: 'Invalid refresh token'})
    } else if (err instanceof JWTExpired) {
      res.status(410).json({err: 'Refresh token expired'})
    } else if (err instanceof errors.SessionNotFoundError) {
      res.status(404).json({err: 'Refresh token not found'})
    } else {
      next(err)
    }
  }
})

api.get('/', (req, res) => {
  res.status(200).json({version})
})

api.use((req, res, next) => {
  res.status(404).json({err: 'Not Found'})
})

api.use((err, req, res, next) => {
  if (err.type === 'request.aborted') {
    res.end()
  } else if (err.type === 'entity.parse.failed') {
    res.status(400).json({err: 'Malformed Body'})
  } else if (err.type === 'charset.unsupported') {
    res.status(400).json({err: 'Charset Unsupported'})
  } else {
    logger.error({
      msg: 'Internal error',
      err,
    })
    res.status(500).json({err: 'Internal Error'})
  }
})

module.exports = api
