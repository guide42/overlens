const pino = require('pino')
const {ENV} = require('./env')
const level = ENV === 'development' ? 'trace' : 'error'
module.exports = pino({level})
