const env = require('./env')
const db = require('./db')

const Challenges = require('./domain/repositories/Challenges')
const Sessions = require('./domain/repositories/Sessions')

const challengeController = require('./domain/controllers/challenge')
const signinController = require('./domain/controllers/signin')
const verifyController = require('./domain/controllers/verify')
const refreshController = require('./domain/controllers/refresh')

const challenges = new Challenges(db)
const sessions = new Sessions(db)

const challenge = challengeController(challenges, env.MESSAGE_TEMPLATE)
const signin = signinController(challenges, sessions, env.MESSAGE_TEMPLATE, env.JWT_SECRET)
const verify = verifyController(sessions, env.JWT_SECRET)
const refresh = refreshController(sessions, env.JWT_SECRET)

module.exports = {
  challenge,
  signin,
  verify,
  refresh,
}
